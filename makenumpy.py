import os
import numpy as np
import torch
from google.cloud import storage
import glob

def read_data_from_file(file_name):
    return torch.from_numpy(np.loadtxt(file_name, delimiter='\t')).float()

data_dir = '/home/inara/inara-debug-10/train'
files = glob.glob(os.path.join(data_dir, '*.csv'))
data = read_data_from_file(files[0])
print('size of data from one file:'+str(len(data)))

#Nt = len(files)
print('Number of files for training:: ',len(files))
Nt = 100

for i in range(1,int(Nt)):
    print('Adding file number '+str(i))
    data = np.hstack((data, read_data_from_file(files[i])))
    #print(data)
    np.save('pred/train_x_'+str(Nt)+'_np.npy', np.array(data))

np.array(data).shape
